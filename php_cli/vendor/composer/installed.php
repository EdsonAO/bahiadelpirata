<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '8085ef55713b9b71a23aa9074f86fb2e113759be',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '8085ef55713b9b71a23aa9074f86fb2e113759be',
    ),
    'php-amqplib/php-amqplib' => 
    array (
      'pretty_version' => 'v2.12.1',
      'version' => '2.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0eaaa9d5d45335f4342f69603288883388c2fe21',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.29',
      'version' => '2.0.29.0',
      'aliases' => 
      array (
      ),
      'reference' => '497856a8d997f640b4a516062f84228a772a48a8',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd87d5766cbf48d72388a9f6b85f280c8ad51f981',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.1.7',
      'version' => '5.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3a2e64866169586502f0cd9cab69135ad12cee9',
    ),
    'videlalvaro/php-amqplib' => 
    array (
      'replaced' => 
      array (
        0 => 'v2.12.1',
      ),
    ),
  ),
);
