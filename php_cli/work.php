<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpParser\Node\Expr\Cast\String_;
use Symfony\Component\Process\Process;

$connection = new AMQPStreamConnection('localhost', 5672,'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare(
    $queue = 'Videos',
    $passive = false,
    $durable = true,
    $exclusive = false,
    $auto_delete = false,
    $nowait = false,
    $arguments = null,
    $ticket = null
);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($msg){
    
    $db_connect = pg_connect("host=localhost port=5432 dbname=bahia_pirata user=postgres password=edson2018");

    echo " [x] Received ", $msg->body, "\n";
    
    $video = json_decode($msg->body, $assocForm=true);
    $usuario = pg_query($db_connect, "SELECT * FROM public.users WHERE id =" . $video['user_id']. ";");
    $user = pg_fetch_row($usuario);
    $ds = DIRECTORY_SEPARATOR;
    
    $file_path = $video['file']. $ds . $user[1].'/%(title)s.%(ext)s';
    pg_query($db_connect, "UPDATE public.videos set status= 'in_progress' WHERE id =" . $video['id']. ";");

    
    
    try {

        $video_url = $video['url'];
        $video_format =$video['format'];
        $query = "SELECT * FROM public.videos WHERE user_id !=" . $video['user_id']. " AND url= '" . $video_url . "' AND format='" . $video_format. "';";
        
        $response = pg_query($db_connect, $query);
        $video_existed = pg_fetch_row($response);

        if($video_existed == false){
            $process = new Process([
                'youtube-dl',
                '-f',
                'best',
                $video['url'],
                '-o',
                $file_path,
                '--print-json'
            ]);
            $process->setTimeout(2400);
            $process->mustRun();
    
            if (!$process->isSuccessful()) {
                pg_query($db_connect, "UPDATE public.videos set status= 'failed' WHERE id =" . $video['id']. ";");
            }
            else{
    
                $output = json_decode($process->getOutput(), true);
                $new_file_path = $video['file']. $ds . $user[1] . $ds . $output['title'] . $video['format'];
                $file_name = str_replace ( $video['file']. $ds . $user[1] . $ds , '' , $new_file_path);
                
                if($video_format != '.mp4'){
                    $process = new Process([
                        'ffmpeg',
                        '-i',
                        $output['_filename'],
                        $new_file_path
                    ]);
                    $process->setTimeout(2400);
                    $process->mustRun();

                    if (!$process->isSuccessful()) {
                        pg_query($db_connect, "UPDATE public.videos set status= 'failed' WHERE id =" . $video['id']. ";");
                    }
                    else{
        
                        $process = new Process([
                            'rm',
                            $output['_filename']
                        ]);
                        $process->setTimeout(2400);
                        $process->mustRun();
                        pg_query($db_connect, "UPDATE public.videos set file_name= '" . $file_name. "',file= '" . $new_file_path. "', status= 'completed' WHERE id =" . $video['id']. ";");

                    }
                }
                pg_query($db_connect, "UPDATE public.videos set file_name= '" . $file_name. "',file= '" . $new_file_path. "', status= 'completed' WHERE id =" . $video['id']. ";");
                if (json_last_error() !== JSON_ERROR_NONE) {
                    throw new \Exception("Could not download the file!");
                    pg_query($db_connect, "UPDATE public.videos set status= 'failed' WHERE id =" . $video['id']. ";");
                }
            }

        }else{

            $file_user = $video['file']. $ds . $user[1];
            $new_file_video = $file_user . $ds .$video_existed[4];

            if(file_exists($file_user)==false){
                $process = new Process([
                    'mkdir',
                    $file_user
                ]);
                $process->setTimeout(2400);
                $process->mustRun();
            }
            

            $process = new Process([
                'ln',
                $video_existed[3],
                $file_user
            ]);
            $process->setTimeout(2400);
            $process->mustRun();
            pg_query($db_connect, "UPDATE public.videos set file= '" . $new_file_video. "',file_name= '" . $video_existed[4]. "', status= 'completed' WHERE id =" . $video['id']. ";");
            

        }
        

    }  catch (\Throwable $exception) {
        pg_query($db_connect, "UPDATE public.videos set status= 'failed' WHERE id =" . $video['id']. ";");
        echo $exception->getMessage();
        
    }
    
    
    echo " [x] Done", "\n";
    echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 5, null);

$channel->basic_consume(
    $queue = 'Videos',
    $consumer_tag = '',
    $no_local = false,
    $no_ack = false,
    $exclusive = false,
    $nowait = false,
    $callback
);

while (count($channel->callbacks)) 
{
    $channel->wait();
}

$channel->close();
$connection->close();