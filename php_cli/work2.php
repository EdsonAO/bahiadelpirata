<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;	
use YoutubeDl\Options;
use YoutubeDl\YoutubeDl;
use Symfony\Component\Process\Process;

$connection = new AMQPStreamConnection('localhost', 5672,'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare(
    $queue = 'Videos',
    $passive = false,
    $durable = true,
    $exclusive = false,
    $auto_delete = false,
    $nowait = false,
    $arguments = null,
    $ticket = null
);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($msg){
    $db_connect = pg_connect("host=localhost port=5432 dbname=bahia_pirata user=postgres password=edson2018");

    echo " [x] Received ", $msg->body, "\n";
    
    $video = json_decode($msg->body, $assocForm=true);
    $usuario = pg_query($db_connect, "SELECT * FROM public.users WHERE id =" . $video['user_id']. ";");
    $user = pg_fetch_row($usuario);
    $ds = DIRECTORY_SEPARATOR;
    
    $file_path = $video['file']. $ds . $user[1].'/%(title)s.%(ext)s';
    echo $file_path;
    $result = pg_query($db_connect, "UPDATE public.videos set status= 'in_progress' WHERE id =" . $video['id']. ";");
    try {
        $process = new Process([
            'youtube-dl',
            $video['url'],
            '-o',
            $file_path
            , '--print-json'
        ]);
        $process->setTimeout(2400);
        $process->mustRun();
        if (!$process->isSuccessful()) {
            $result = pg_query($db_connect, "UPDATE public.videos set status= 'failed' WHERE id =" . $video['id']. ";");
        }else{
            $output = json_decode($process->getOutput(), true);
            $result = pg_query($db_connect, "UPDATE public.videos set file= '" . $output['_filename']. "', status= 'completed' WHERE id =" . $video['id']. ";");
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \Exception("Could not download the file!");
                $result = pg_query($db_connect, "UPDATE public.videos set status= 'failed' WHERE id =" . $video['id']. ";");
            }
        }

    }  catch (\Throwable $exception) {
        $result = pg_query($db_connect, "UPDATE public.videos set status= 'failed' WHERE id =" . $video['id']. ";");
        echo $exception->getMessage();
        
    }
    
    
    echo " [x] Done", "\n";
    echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 5, null);

$channel->basic_consume(
    $queue = 'Videos',
    $consumer_tag = '',
    $no_local = false,
    $no_ack = false,
    $exclusive = false,
    $nowait = false,
    $callback
);

while (count($channel->callbacks)) 
{
    $channel->wait();
}

$channel->close();
$connection->close();