<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VideoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->post('/download', [VideoController::class, 'download'])->name('download');

Route::middleware(['auth:sanctum', 'verified'])->get('/videos/all', [VideoController::class, 'all'])->name('all');

Route::middleware(['auth:sanctum', 'verified'])->get('/videos', [VideoController::class, 'index'])->name('videos');

Route::middleware(['auth:sanctum', 'verified'])->post('/videos/create', [VideoController::class, 'create'])->name('create');

Route::middleware(['auth:sanctum', 'verified'])->get('/videos/delete', [VideoController::class, 'delete'])->name('delete');

Route::middleware(['auth:sanctum', 'verified'])->post('/videos/watch', [VideoController::class, 'watch'])->name('watch');

Route::middleware(['auth:sanctum', 'verified'])->post('/download/droxbox', [VideoController::class, 'droxbox'])->name('droxbox');