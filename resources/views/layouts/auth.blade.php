
<link href="{{ asset('/css/logreg.css') }}" rel="stylesheet">

<!-- Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

<!-- Styles -->
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<body class="body">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <a class="navbar-brand" href="{{ route('videos') }}">
            <img src="{{asset('imgs/logo.PNG')}}" id="logo" alt="">
          </a> 
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          </ul>
          <ul class="nav justify-content-end">
            <li class="nav-item">
                @if (Route::has('login'))
                    <div id="btns">
                        @auth
                            @else
                                <a href="{{ route('login') }}" class="text-sm text-white-700 btn" id="btn">Login</a>       
                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="ml-2 text-sm text-white-700 btn" id="btn">Register</a>
                            @endif
                        @endif
                        @if(Auth::check())
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <x-jet-dropdown-link id="btn" href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                                this.closest('form').submit();">
                                    {{ __('Logout') }}
                                </x-jet-dropdown-link>
                            </form>
                        @endif
                        
                    </div>
                
                @endif
            </li>
          </ul>
        </div>
        
      </nav>
    <div class="relative flex items-top justify-center sm:items-center sm:pt-0" id="auth">
        
        @yield('content')
    </div>
</body>
