@extends('layouts.auth')

@section('content')
        <x-guest-layout>
        <div class="flex sm:justify-center items-center  bg-dark-100" id="loguearse">
            <x-slot name="logo">
                
            </x-slot>
            <x-jet-validation-errors class="mb-4" />
            @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif
            <div class="container" id="form_login">
                <form method="POST" action="{{ route('login') }}" id="form_login">
                    @csrf
                    <h1 class="text-center text text-lg">Login</h1>
                    <div>
                        <x-jet-label for="email"  class="text"  value="{{ __('Email') }}" />
                        <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                    </div>
        
                    <div class="mt-4">
                        <x-jet-label for="password"   class="text"  value="{{ __('Password') }}" />
                        <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
                    </div>
        
                    <div class="block mt-4">
                        <label for="remember_me" class="flex items-center text" >
                            <input id="remember_me" type="checkbox" class="form-checkbox text" name="remember">
                            <span class="ml-2 text-sm text text">{{ __('Remember me') }}</span>
                        </label>
                    </div>
        
                    <div class="flex items-center justify-end mt-4">
                        @if (Route::has('password.request'))
                            <a class="underline text-sm text hover:text-white-900" href="{{ route('password.request') }}">
                                {{ __('Forgot your password?') }}
                            </a>
                        @endif
        
                        <x-jet-button class="ml-4">
                            {{ __('Login') }}
                        </x-jet-button>
                    </div>
                </form>
            </div>
        </div>
    </x-guest-layout>
@endsection
   
