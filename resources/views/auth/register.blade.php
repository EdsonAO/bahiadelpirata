@extends('layouts.auth')
@section('content')

<x-guest-layout>
    <div class="flex sm:justify-center items-center  bg-dark-100" id="loguearse">
        <x-slot name="logo">
        </x-slot>

        <x-jet-validation-errors class="mb-4" />
        <div class="container" id="form_register">
            <form method="POST" action="{{ route('register') }}" id="form_register">
                @csrf
                <h1 class="text-center text text-lg">Register</h1>
                <div>
                    <x-jet-label for="name" class="text" value="{{ __('Name') }}" />
                    <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="email"  class="text" value="{{ __('Email') }}" />
                    <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                </div>

                <div class="mt-4">
                    <x-jet-label for="password"  class="text" value="{{ __('Password') }}" />
                    <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="password_confirmation"  class="text" value="{{ __('Confirm Password') }}" />
                    <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
                </div>

                <div class="flex items-center justify-end mt-4">
                    <a class="underline text-sm text hover:text-white-900" href="{{ route('login') }}">
                        {{ __('Already registered?') }}
                    </a>

                    <x-jet-button class="ml-4 btn-success">
                        {{ __('Register') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-guest-layout>
@endsection

