<body>
    <x-app-layout>
        <div class="py-12">
            @if ($errors->any())
                <div class="alert alert-danger my_alert text-center">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-primary text-center">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success text-center">
                    {{ session('success') }}
                </div>
            @endif
            <div class="videos">
                <div class="d-flex">
                    <form method="POST" action="{{ route('create') }}" autocomplete="off">
                        @csrf
                        <label for="" class="text-dark title_url">Video URL</label>
                        <input type="text" id="url" class="my_imput text-center" name="url">
                        <select name="format" id="my_format" class="custom-select">
                            <option selected disabled value="">Format</option>
                            <option>.mp4</option>
                            <option>.avi</option>
                            <option>.flv</option>
                            <option>.mov</option>
                            <option>.mkv</option>
                            <option>.ogg</option>
                        </select>
                        <div class='container'>
                            <button type="submit" class="btn btn_submit">START</button>
                          </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="btn_del">
            <a href="{{route('delete')}}" class="btn btn_delete"><i class="far fa-trash-alt"></i></a>
            </div>
            <table class="table" id="videos" style="width: 1172px;">
                <thead>
                    <tr id="titles" class="text-center">
                        <th scope="col">Link</th>
                        <th scope="col">Status</th>
                        <th scope="col">Format</th>
                        <th scope="col">Download</th>
                        <th scope="col">Droxbox</th>
                        <th scope="col">Watch</th>
                    </tr>
                </thead>
            </table>
        </div>
    </x-app-layout>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script type="text/javascript">
    function cargarVideos(){
        var tabla = $('#videos').DataTable({
             "paging": false,
             searching: false,
             destroy: true,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "serverSide": true,
            "ajax": "{{ url('/videos/all') }}",
            "columns": [
                {data: 'url'},
                {
                    "data": "status",
                    "render": function (data, type, row, meta) {
                        var html = '<h3 class="text-center">'+data+'</h3>';
                        return html;
                    }
                },
                {
                    "data": "format",
                    "render": function (data, type, row, meta) {
                        var html = '<h3 class="text-center">'+data+'</h3>';
                        return html;
                    }
                },
                {
                    "data": "file",
                    "render": function (data, type, row, meta) {
                        var html = '<div class="text-center"><form method="POST" action={{route("download")}}>';
                        html += '@csrf'
                        html += '<input type="hidden" name="file" value="'+data+'">';
                        html += '<button type="submit" class="btn btn-success"><i class="fas fa-download"></i></button>';
                        html += '</form></div>';
                        return html;
                    }
                },
                {
                    "data": "file",
                    "render": function (data, type, row, meta) {
                        var html = '<div class="text-center text-primary"><form method="POST" action={{route("droxbox")}}>';
                        html += '@csrf'
                        html += '<input type="hidden" name="file" value="'+data+'">';
                        html += '<button type="submit" class="btn btn-primary"><i class="fab fa-dropbox"></i></button>';
                        html += '</form></div>';
                        return html;
                    }
                },
                {
                    "data": "id",
                    "render": function (data, type, row, meta) {
                        var html = '<div class="text-center"><form method="POST" action={{route("watch")}}>';
                        html += '@csrf'
                        html += '<input type="hidden" name="id_video" value="'+data+'">';
                        html += '<button type="submit" class="btn btn-dark"><i class="fas fa-eye"></i></button>';
                        html += '</form></div>';
                        return html;
                    }
                }
            ]
            
        });
    }
    $( document ).ready(function () {
        cargarVideos();
        setInterval( cargarVideos , 5000);
    });
</script>