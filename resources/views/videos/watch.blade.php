<body>
    
    <x-app-layout>
    <a href="{{route('videos')}}" class="btn btn_back text-center"><i class="fas fa-arrow-square-left"></i></a>
        <div class="py-12">
            <div class="condtainer">
                <video  controls autoplay >
                    <source src="{{asset($file_watch)}}" type="video/mp4">
                    <source src="{{asset($file_watch)}}" type="video/avi">
                    <source src="movie.ogg" type="video/ogg">
                </video>
                <div class="text-center">
                    <form method="POST" action={{route("download")}}>
                        @csrf
                        <input type="hidden" name="file" value="{{$video->file}}">
                        <button type="submit" id="download" class="btn btn-success">
                            <i class="fas fa-download"></i>
                        </button>
                    </form>
                </div>
                
            </div>
        </div>
    </x-app-layout>
</body>