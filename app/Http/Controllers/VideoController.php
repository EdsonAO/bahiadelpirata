<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Video;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use \Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use Spatie\Dropbox\Client;
use Spatie\Dropbox\WriteMode;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;

class VideoController extends Controller
{
    public function index(){
        return view('videos.index');
    }
    public function all(Request $request){
        $user = Auth::user();
        $videos = Video::where('user_id',$user->id)->get();
        return Datatables::of($videos)->make(true);
    }

    public function delete(){
        $user = Auth::user();
        Video::where('user_id', $user->id)->delete();
        return back();
    }

    public function download(Request $request){
        $request->validate([
            'url' => 'file',
        ]);

        return response()->download($request->file);
    }

    public function droxbox(Request $request){
        $request->validate([
            'url' => 'file',
        ]);
        $dropboxKey ="gvgaawase55ecq4";
        $dropboxSecret ="mjtrhwhwmj5vnka";
        $dropboxToken="sl.Ak2HT-VBuhDOXv1NK2KTuerTFah4YtQD1dUVHAy3zbdw32IkUuPo5_dZer-YpNrmqxijsG1ePI93ZbEtYjWJbrFWp334U098m0IHulsYCNpyc5SzICOxHF6r3zQMacAWa5s5n_M";


        $app = new DropboxApp($dropboxKey,$dropboxSecret,$dropboxToken);
        $dropbox = new Dropbox($app);

        try{
            $user = Auth::user();
            $ds = DIRECTORY_SEPARATOR;
            $tempfile = $request->file;
            $UBI = $ds. 'home' .$ds. 'edson' .$ds. 'Documentos' .$ds. 'bahiadelpirata' .$ds. 'public' .$ds .'descargas' . $ds . $user->name . $ds;
            $video_name = str_replace ( $UBI , '' , $tempfile);
            $dropbox->simpleUpload( $tempfile,'/'.$video_name, ['autorename' => true]);
            return back()->with('status','Video downloaded successfully!!');
        }catch(\exception $e){
            return back()->with('success',$e);
                
        }
    }

    public function watch(Request $request){
        $request->validate([
            'url' => 'id_video',
        ]);
        $video = Video::find($request->id_video);
        $ds = DIRECTORY_SEPARATOR;
        //$UBI = 'C:' .$ds. 'Users' .$ds. 'edson' .$ds. 'OneDrive' .$ds. 'Documentos' .$ds. 'web' .$ds. 'public' .$ds;
        $UBI = $ds. 'home' .$ds. 'edson' .$ds. 'Documentos' .$ds. 'bahiadelpirata' .$ds. 'public' .$ds;
        $file_watch = str_replace ( $UBI , '' , $video->file);
        return view('videos.watch', compact('video','file_watch'));
    }


    public function create(Request $request){
        $request->validate([
            'url' => 'required',
            'format' => 'required',
        ]);

        $user = Auth::user();

        if(Video::where([
            ['user_id',$user->id,],
            ['url' , $request->url ],
            ['format',$request->format],
            ['status','completed']
            ])->exists())
        {

            return back()->with('status','This video was previously downloaded in this format!!');
        }

        $video = new Video;
        $video->url = $request->url;
        $video->format = $request->format;
        $video->user_id = $user->id;
        $video->file = public_path('descargas');
        $video->save();

        
        $connection = new AMQPStreamConnection('localhost', 5672,'guest', 'guest');
        $channel = $connection->channel();
        $channel->queue_declare('Videos', true, false, false, false);
        $message = $video;
        $msg = new AMQPMessage($message);
        $channel->basic_publish($msg, '', 'Videos');
        $channel->close();
        $connection->close();
        
        return back();
    }
}
